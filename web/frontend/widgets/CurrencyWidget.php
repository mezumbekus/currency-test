<?php

namespace frontend\widgets;

use yii\base\Exception;
use yii\base\Widget;
use Yii;
use frontend\models\TradeProvider;

/**
 * Class CurrencyWidget
 * @package frontend\widgets
 * Виджет для отображения курса валют
 */

class CurrencyWidget extends Widget
{
    public $currency;

    public function run()
    {
        $trades = Yii::$app->params['currencyList'];
        foreach ($trades as $trade => $url) {
            $data = (TradeProvider::getTradeInstance($trade,$url))->getCurrencyValue($this->currency);
            if($data !== null)
                return $this->render('index',[
                   'data' => $data,
                    'label' => join('=>',$this->currency),
                ]);
        }
        throw new Exception('Нет рабочих бирж');
    }

}