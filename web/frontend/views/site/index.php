<?php

/* @var $this yii\web\View */


$this->title = 'Currency Test';
use frontend\widgets\CurrencyWidget;
$this->registerJsFile('js/main.js',['position' => yii\web\View::POS_END]);
?>
<div class="site-index">

<?= CurrencyWidget::widget(['currency' => ['EUR','RUB']]) ?>

</div>
