<?php
namespace frontend\models;

class EcbTrade extends TradeProvider
{
    protected function saveData()
    {
        $data = simplexml_load_file($this->url)->Cube->Cube->Cube;
        foreach ($data as $el){
            $this->data[$el->attributes()->currency->__toString()] = $el->attributes()->rate->__toString();
        }

    }

    public function getCurrencyValue($val)
    {
        foreach ($val as $el) {
            if(isset($this->data[$el]))
                return $this->data[$el];
        }
        return null;
    }
}

