<?php

namespace frontend\models;

class CbrTrade extends TradeProvider
{
    protected function saveData()
    {
        $this->data = json_decode(file_get_contents($this->url))->Valute;
    }

    public function getCurrencyValue($val)
    {
        foreach ($val as $el) {
            if(isset($this->data->$el))
                return $this->data->$el->Value;
        }
        return null;
    }

}

