<?php

namespace frontend\models;

abstract class TradeProvider
{
    protected $url;
    protected $data;

    public function __construct($url = null)
    {
        $this->url = $url;
        $this->saveData();
    }
    abstract protected function saveData();

    abstract public function getCurrencyValue($val);


    public static function getTradeInstance($trade,$url)
    {
        $fullPath = __NAMESPACE__.'\\'.$trade;
        return new $fullPath($url);
    }
}

