<?php
namespace frontend\controllers;


use yii\web\Controller;
use frontend\widgets\CurrencyWidget;
/**
 * Site controller
 */
class SiteController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCurrency()
    {
        return CurrencyWidget::widget(['currency' => ['EUR','RUB']]);
    }

}
